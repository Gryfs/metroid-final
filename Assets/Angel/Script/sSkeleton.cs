﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sSkeleton : MonoBehaviour
{
    public Transform target;
    public float speed;
    public float stoppingDistance;
    private Boolean awake;
    public Animator animator;
    public int mobDamage = 1;
    public float agroRange;

    private Rigidbody2D rb;


    public bool isFlipped = false;

    void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        
        rb = GetComponent<Rigidbody2D>();
    }





    // Update is called once per frame
    void Update()
    {
        //target = GameObject.FindGameObjectWithTag("Player1").GetComponent<Transform>();

      /* float distPlayer = Vector2.Distance(transform.position, target.position);

        if (distPlayer < agroRange)
        {
            ChasePlayer();
        }
        else
        {
            StopChasePlayer();
        }
        
        LookAtTarget();*/

    }



    public void LookAtTarget()
    {
        Vector3 flipped = transform.localScale;
        flipped.z *= -1f;

        if (transform.position.x > target.position.x && isFlipped)
        {
            transform.localScale = flipped;
            transform.Rotate(0f, 180f, 0f);
            isFlipped = false;
        }
        else if (transform.position.x < target.position.x && !isFlipped)
        {
            transform.localScale = flipped;
            transform.Rotate(0f, 180f, 0f);
            isFlipped = true;
        }
    }



    private void ChasePlayer()
    {
        

        if (transform.position.x < target.position.x)
        {
            rb.velocity = new Vector2(speed, 0);
        }
        else 
        {
            rb.velocity = new Vector2(-speed, 0);
        }
      
    }

    private void StopChasePlayer()
    {
        rb.velocity = Vector2.zero;
    }
}
