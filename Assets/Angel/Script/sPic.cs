﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sPic : MonoBehaviour
{
    public int picDamage = 1;
    private Transform target;
    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        void OnTriggerEnter2D(Collider2D other)
        {
            Player1 enemy = GetComponent<Player1>();
            if (other.gameObject.CompareTag("Player"))
            {
                if (enemy != null)
                {
                    enemy.TakeDamage(picDamage);
                }
            }
        }
    }
    
}
