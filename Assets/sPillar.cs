﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sPillar : MonoBehaviour
{

    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("shooter"))
        {
            Destroy(other.gameObject);
            Destroy(this.gameObject);
        }
    }
}
