﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : MonoBehaviour
{
    public float minimum2;
    public float maximum2;
    public float t2;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.position =
            new Vector3(Mathf.Lerp(minimum2, maximum2, t2), transform.position.y, transform.position.z);
        t2 += 0.6f * Time.deltaTime;

        if (t2 > 1.0f)
        {
            float temp = maximum2;
            maximum2 = minimum2;
            minimum2 = temp;
            t2 = 0.0f;
        }
    }
}
