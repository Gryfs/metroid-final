﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSword : MonoBehaviour
{
    public float speed = 20f;
    public int damage = 1;
    public Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        
        rb.velocity = transform.right * speed; //le tir avance vers l'avant
    }

    private void OnTriggerEnter2D(Collider2D hitInfo)
    {
        EnemyDamage enemy = hitInfo.GetComponent<EnemyDamage>(); //application du take damage dans le script de l'enemy qui gère sa vie pour lui infliger des dmg
        if (enemy != null)
        {
            enemy.TakeDamage(damage);
        }
        Destroy(gameObject); //disparition après avoir toucher
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject); //destruction du projectile lorsque le personnage sort de l'écran
    }
    // Update is called once per frame
}
