﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;

    public GameObject PauseMenuUi;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();               //parametre du "echap" pour savoir si il faut arreter le menu pause ou le mettre
            }
            else
            {
                Pause();
            }
        }
    }
    
    public void Resume()
    {
        PauseMenuUi.SetActive(false);
        Time.timeScale = 1f; //le temps reprend son cour
        GameIsPaused = false;
    }

    void Pause()
    {
        PauseMenuUi.SetActive(true);
        Time.timeScale = 0f;  //affichage pause menu et temps fige
        GameIsPaused = true;
    }
    public void Restart()
    {
        SceneManager.LoadScene("SampleScene"); //restart la partie
    }

    public void QuitGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1); //retoyr au menu
        
    }
}
