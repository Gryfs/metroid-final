﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Player1 : MonoBehaviour
{
    public bool isShooting = false;
    public GameObject otherPlayer;

    public float speed = 5.0f;
    private Rigidbody2D rb;
    

    private int extraJumps;
    public int extraJumpsValue;
    public float jumpForce;

    private bool isGrounded;
    public Transform groundCheck;
    public float checkRadius;
    public LayerMask whatIsGround;

    private bool facingRight = true;
    private int facingDirection = 1;


    public float timeBtwAttack;
    public float startTimeBtwAttack;

    public Transform attackPos;
    public LayerMask whatIsEnemies;
    public float attackRange;
    public int damage = 1;

    public bool attack;
    public Animator animator;

    public int numOfHearts;
    public Image[] hearts;
    public Sprite fullHeart;
    public Sprite emptyHeart;
    public int health = 5;

    public GameObject Restart;
    public GameObject GameOver;

    public bool invincible = false;

    public Transform wallCheck;
    private bool isTouchingWall;
    public float wallCheckDistance;
    private bool isWallSliding;
    public float wallSlideSpeed;

    public float knockback;
    public float knockbackLength;
    public float knockbackCount;
    public bool knockFromRight;
    Vector3 movement;
    SpriteRenderer sprite;


    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        
        extraJumps = extraJumpsValue; //gerer via l inspector le nomvbre de saut
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround); //mise en place de la detection du sol
        
        isTouchingWall = Physics2D.Raycast(wallCheck.position, transform.right, wallCheckDistance, whatIsGround); //mise en place de la detection du mur

        if (isWallSliding)          //mise en place de la vitesse de chute réduite quand coller au mur
        {
            if (rb.velocity.y < -wallSlideSpeed)
            {
                rb.velocity = new Vector2(rb.velocity.x, -wallSlideSpeed); //reduction vitesse
                
            }
        }

        

        CheckIfWallSliding();

       


        if (!this.animator.GetCurrentAnimatorStateInfo(0).IsTag("Attack")) //le personnage peut marcher lorsqu'il n'y a pas d'animation d'attaque
        {
            movement = new Vector3(Input.GetAxis("Horizontal"), 0f, 0f); //déplacement
            transform.position += movement * Time.deltaTime * speed;
            animator.SetFloat("Speed", Mathf.Abs(movement.x));
        }
        else if (this.animator.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))  //le personnage ne peut pas avancer pendant l'animation d'attaque
        {
            rb.velocity = new Vector2(0, 0);

        }

        if ((isGrounded == true)) // si le perso touche le sol
        {

            extraJumps = extraJumpsValue;
            if (rb.velocity.y < 0) //et que sa velocité en y est de 0 ou inférieur alors on stop l'a    nimation du jump
            {
                animator.SetBool("IsJumping", false);

            }
        }



        if (Input.GetKeyDown(KeyCode.UpArrow) && extraJumps == 0 && isGrounded == true) //mise en place du jump en fonction de la detection du sol
        {
            rb.velocity = Vector2.up * jumpForce * 1.5f;
            animator.SetBool("IsJumping", true);

        }

        if (facingRight == false && movement.x > 0)  //changement de la direction du sprite en fonction du deplacement
        {
            Flip();
        }
        else if (facingRight == true && movement.x < 0)
        {
            Flip();
        }

        if (timeBtwAttack <= 0) //mise en place d'un temps entre les attaque
        {
            if (Input.GetKeyDown(KeyCode.Space) && isGrounded) //attaque
            {
                Attack();

            }
            if (isGrounded == false && Input.GetKeyDown(KeyCode.Space))  //attaque en l'air
            {
                JumpAttack();
            }
        }
        else
        {

            timeBtwAttack -= Time.deltaTime; //écoulement du temps entre les attaques
        }





        if (health > numOfHearts)  //mise en place du système de coeurs (affichage)
        {
            health = numOfHearts;
        }

        for (int i = 0; i < hearts.Length; i++)
        {
            if (i < health)
            {
                hearts[i].sprite = fullHeart;
            }
            else
            {
                hearts[i].sprite = emptyHeart;
            }


            if (i < numOfHearts)
            {
                hearts[i].enabled = true;
            }
            else
            {
                hearts[i].enabled = false;
            }
        }

        if (invincible && health > 0) //Mise en place des frames ou le personnage est invincible 
        {
            StartCoroutine("GetInvulnerable");
        }






    }
    private void CheckIfWallSliding() //verification que le joueur touche le mur et non le sol
    {
        if (isTouchingWall && !isGrounded)
        {
            isWallSliding = true;
        }
        else
        {
            isWallSliding = false;
        }
    }
    IEnumerator GetInvulnerable()
    {
        invincible = true;  //animation de dégats avec clignotements rouge

        animator.Play("Player_Hurt");
        sprite.color = new Color(1, 0, 0, 1);
        yield return new WaitForSeconds(0.2f);
        sprite.color = new Color(1, 1, 1, 1);
        yield return new WaitForSeconds(0.2f);
        sprite.color = new Color(1, 0, 0, 1);
        yield return new WaitForSeconds(0.2f);
        sprite.color = new Color(1, 1, 1, 1);
        yield return new WaitForSeconds(0.2f);
        sprite.color = new Color(1, 0, 0, 1);
        yield return new WaitForSeconds(0.2f);
        sprite.color = new Color(1, 1, 1, 1);
        sprite.color = new Color(1, 0, 0, 1);
        yield return new WaitForSeconds(0.2f);
        sprite.color = new Color(1, 1, 1, 1);
        invincible = false;
       
    }

    void JumpAttack()  //animation d'attaque en l'air avec invocation du projectile et mise en place du temps entre les attaque
    {
        animator.Play("Player_JumpAttack");
        Collider2D[] ennemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsEnemies);
        for (int i = 0; i < ennemiesToDamage.Length; i++)
        {
            ennemiesToDamage[i].GetComponent<EnemyDamage>().TakeDamage(damage);

        }
        timeBtwAttack = startTimeBtwAttack;

    }

    void Attack() //pareil que pour l'attaque en l'air mais sur le sol
    {


        animator.SetTrigger("IsAttacking");
        Collider2D[] ennemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsEnemies);
        for (int i = 0; i < ennemiesToDamage.Length; i++)
        {
            if(ennemiesToDamage[i] != null)
            {
                ennemiesToDamage[i].GetComponent<EnemyDamage>().TakeDamage(damage);
            }
            

            
        }
        timeBtwAttack = startTimeBtwAttack;

    }

    public void TakeDamage(int damage) //mise en place du système de dégats pour le player
    {
        if (!invincible)
        {
            health -= damage;
            invincible = true;
            
        }
        
        if (health <= 0)
        {
            StartCoroutine("GetRavaged"); //Mort 
        }
    }

   

    IEnumerator GetRavaged()
    {
        animator.Play("Player_Die"); //animation de mort et affichage du gameover et bouton restart
        yield return new WaitForSeconds(0.6f);
        Destroy(gameObject);
        GameOver.SetActive(true);
        Restart.SetActive(true);        
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        knockbackCount = knockbackLength;

        if (transform.position.x < transform.position.x)  //mise en place d'un knockback en fonction de la direction
        {
            rb.velocity = new Vector2(6, 6);
        }
        else
        {
            knockFromRight = false;
            rb.velocity = new Vector2(-6, 6);
        }
        if (other.gameObject.CompareTag("shooter")) //activation du powerup de tir
        {
            Destroy(other.gameObject);
            isShooting = true;
            otherPlayer.GetComponent<Movement>().isShooting = true;
        }

        if (other.gameObject.CompareTag("enemyBullet"))
        {
            health = health - 1;
        }

        
    }
    

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red; //couleur de la ligne
        Gizmos.DrawWireSphere(attackPos.position, attackRange); //Sphère d'attaque
        Gizmos.DrawLine(wallCheck.position, new Vector3(wallCheck.position.x + wallCheckDistance, wallCheck.position.y, wallCheck.position.z)); //création de la ligne de detection du mur
    }


    void Flip() //changement de direction par le sprite
    {
        facingDirection *= -1;
        facingRight = !facingRight;  // Switch the way the player is labelled as facing.
        Vector3 Scaler = transform.localScale;
        Scaler.x *= -1;
        wallCheckDistance *= -1;
        transform.localScale = Scaler;
    }

}

