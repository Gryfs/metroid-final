﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Changecolour : MonoBehaviour
{
    SpriteRenderer sprite;
    // Start is called before the first frame update
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>(); //changement de couleur du 2ème joueur pour différencier
        sprite.color = new Color(1, 1, 0, 1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
