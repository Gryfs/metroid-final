﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public bool isShooting = true;
    public GameObject fire;
    public float minimum2;
    public float maximum2;
    public float t2;

    void Start()
    {
        InvokeRepeating("SpawnObject2", 1f, 1f);
    }

    void SpawnObject2()
    {
        if (isShooting)
        {
            Instantiate(fire, new Vector3(transform.position.x, transform.position.y, transform.position.z),
                Quaternion.Euler(0, 0, 0));
        }
    }

    void Update()
    {
        transform.position = new Vector3(transform.position.x, Mathf.Lerp(minimum2, maximum2, t2),transform.position.z);
            t2 += 1.3f * Time.deltaTime;

            if (t2 > 1.0f)
            {
                float temp = maximum2;
                maximum2 = minimum2;
                minimum2 = temp;
                t2 = 0.0f;
            }
    }
}