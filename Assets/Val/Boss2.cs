﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss2 : MonoBehaviour
{
    public GameObject bossSplited;
    public int boss2Health;
    public float speed;
    public Rigidbody2D rb;
    public Transform player;
    public bool isFlipped = false;
    public Slider healthBar;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        healthBar.value = boss2Health;
    }

    // Update is called once per frame
    void Update()
    {
        
        BossSplit();
        Vector3 flipped = transform.localScale;
         transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
        if (transform.position.x > player.transform.position.x && isFlipped)
        {
            transform.localScale = flipped;
            transform.Rotate(0f, 180f, 0f);
            isFlipped = false;
        }
        else if (transform.position.x < player.transform.position.x && !isFlipped)
        {
            transform.localScale = flipped;
            transform.Rotate(0f, 180f, 0f);
            isFlipped = true;
        }
        
        healthBar.value = boss2Health;
        
    }

    public void BossSplit()
    {
        if (boss2Health <= 0)
        {
            Destroy(gameObject);
            Debug.Log("fzeqtdst");
            Instantiate(bossSplited);
            Instantiate(bossSplited);
            
        }
    }
}
