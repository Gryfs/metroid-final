﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostBullet : MonoBehaviour
{
    public float moveSpeed = 7f;

    public Rigidbody2D rb;

    public GameObject target;

    Vector2 moveDirection;
    // Start is called before the first frame update
    void Start()
    {
       // target = GameObject.FindGameObjectWithTag("Range").GetComponent<Transform>();
        moveDirection = (target.transform.position - transform.position).normalized * moveSpeed;
        rb.velocity = new Vector2(moveDirection.x,moveDirection.y);
        Destroy(gameObject,3f);
    }

    // Update is called once per frame
    public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name.Equals("Cible"))
        {
            Debug.Log("touché");
            Destroy(gameObject);
        }
    }
}
