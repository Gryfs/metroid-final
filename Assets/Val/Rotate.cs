﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public float turnRotate;
    public bool positiv;
    
    void Update()
    {
        if (transform.eulerAngles.z <= 1)
        {
            positiv = true;
        }
        else if(transform.eulerAngles.z >= 180)
        {
            positiv = false;
        }
        if (positiv) 
        {
            transform.Rotate(Vector3.forward, turnRotate * Time.deltaTime);
        }
        if(!positiv)
        {
            transform.Rotate(Vector3.back, turnRotate * Time.deltaTime);
        }
    }
}
