using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _Bullet : MonoBehaviour
{

	public float speed = 20f;
	public Rigidbody2D rb;
	public GameObject boss2Hp;
	public GameObject bossHp;
	// Use this for initialization
	void Start()
	{
		rb.velocity = transform.right * speed;
		bossHp = GameObject.FindGameObjectWithTag("Boss");
		boss2Hp = GameObject.FindGameObjectWithTag("Boss2");
	}

	void Update()
	{
		Destroy(gameObject, 2f);
	}

	public void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("Boss"))
		{
			bossHp.GetComponent<Boss>().bossHealth -= 5;
			Destroy(this.gameObject);
		}
		if (other.gameObject.CompareTag("Boss2"))
		{
			boss2Hp.GetComponent<Boss2>().boss2Health -= 5;
			Destroy(this.gameObject);
		}
	}
}