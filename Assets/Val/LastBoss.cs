﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class LastBoss : MonoBehaviour
{
    public Rigidbody2D rb;
    public GameObject otherBoss;
    public GameObject spawner;
    public GameObject otherSpawner;
    public int bossHealth;

    // Start is called before the first frame update
    void Start()
    {
        bossHealth = GetComponent<EnemyDamage>().Health;
    }

    // Update is called once per frame
    void Update()
    {
        if (bossHealth == 5 || otherBoss.GetComponent<LastBoss>().bossHealth == 5)
        {
            otherBoss.GetComponent<BoxCollider2D>().enabled = false;
            gameObject.GetComponent<BoxCollider2D>().enabled = true;
            spawner.GetComponent<Spawner>().isShooting = true;
            otherSpawner.GetComponent<Spawner1>().isShooting = false;
        }

        if (bossHealth == 4 || otherBoss.GetComponent<LastBoss>().bossHealth == 4)
        {
            otherBoss.GetComponent<BoxCollider2D>().enabled = true;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            spawner.GetComponent<Spawner>().isShooting = false;
            otherSpawner.GetComponent<Spawner1>().isShooting = true;
        }

        if (bossHealth == 3 || otherBoss.GetComponent<LastBoss>().bossHealth == 3)
        {
            otherBoss.GetComponent<BoxCollider2D>().enabled = false;
            gameObject.GetComponent<BoxCollider2D>().enabled = true;
            spawner.GetComponent<Spawner>().isShooting = true;
            otherSpawner.GetComponent<Spawner1>().isShooting = false;
        }

        if (bossHealth == 2 || otherBoss.GetComponent<LastBoss>().bossHealth == 2)
        {
            otherBoss.GetComponent<BoxCollider2D>().enabled = true;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            spawner.GetComponent<Spawner>().isShooting = false;
            otherSpawner.GetComponent<Spawner1>().isShooting = true;
        }

        if (bossHealth == 1 || otherBoss.GetComponent<LastBoss>().bossHealth == 1)
        {
            otherBoss.GetComponent<BoxCollider2D>().enabled = false;
            gameObject.GetComponent<BoxCollider2D>().enabled = true;
            spawner.GetComponent<Spawner>().isShooting = true;
            otherSpawner.GetComponent<Spawner1>().isShooting = false;
        }

        if (bossHealth == 0 || otherBoss.GetComponent<LastBoss>().bossHealth == 0)
        {
            Destroy(gameObject);
            Destroy(otherBoss);
        }
    }
}

