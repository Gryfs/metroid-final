﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public Rigidbody2D rb;
    public float speed;

    public bool doubleJumpIsActive = false;

    public bool isGrounded = false;
    public float jumpForce = 200f;
    public LayerMask whatIsGround;
    public Transform groundCheck;
    public int nbJumps;
    public float checkRadius;
    public float playerHp = 3;
    public GameObject otherPlayer;
    public bool isShooting = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround); //mise en place de la detection du sol
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            transform.Translate(Vector3.left * speed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * speed * Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.Z) && isGrounded) //mise en place du jump
        {
            rb.velocity = Vector2.up * jumpForce;
            nbJumps = 2;
        }

        if (doubleJumpIsActive)
        {
            if (Input.GetKeyDown(KeyCode.Z) && nbJumps > 0) //mise en place des jumps supplementaires
            {
                rb.velocity = Vector2.up * jumpForce;
                nbJumps -= 1;
            }
        }
    
        if (playerHp <= 0)
        {
            Debug.Log("game over");
        }
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("doubleJump"))
        {
            Destroy(other.gameObject);
            doubleJumpIsActive = true;
            otherPlayer.GetComponent<Movement>().doubleJumpIsActive = true;
        }
        if (other.gameObject.CompareTag("shooter"))
        {
            Destroy(other.gameObject);
            isShooting = true;
            otherPlayer.GetComponent<Movement>().isShooting = true;
        }
        if (other.gameObject.CompareTag("enemyBullet"))
        {
            playerHp -= 1;
        }
    }    
}