﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class Bullet : MonoBehaviour
{
    public float speed = 5f;
    public Rigidbody2D rd;
    void Start()
    {
        rd.velocity = transform.right * speed;
        Destroy(gameObject, 3);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("hit");
            Destroy(gameObject);
        }
    } 
}