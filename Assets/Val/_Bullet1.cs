using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _Bullet1 : MonoBehaviour
{
	public float speed;
	public Rigidbody2D rb;
	// Use this for initialization
	void Start()
	{
		rb.velocity = transform.right * speed * Time.deltaTime;
	}

	void Update()
	{
		Destroy(gameObject, 15f);
	}

	public void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("Mêlée") || other.gameObject.CompareTag("Player2"))
		{
			Destroy(this.gameObject);
		}
	}
}